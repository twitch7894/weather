export const Description = {
  "clear sky": "Cielo Despejado",
  "few clouds": "Pocas Nubes",
  "scattered clouds": "Nubes Dispersas",
  "broken clouds": "Nubes Rotas",
  "shower rain": "lluvioso",
  rain: "lluvia",
  thunderstorm: "Tormenta",
  snow: "Nieve",
  mist: "Neblina",
};

export const Weekday = {
  0: "Domingo",
  1: "Lunes",
  2: "Martes",
  3: "Miercoles",
  4: "Jueves",
  5: "Viernes",
  6: "Sabado",
};
