import { ENDPOINTS } from "../config/endpoints";

describe("Testing Api", () => {
  test("Test weatherForecast in Argentina", async () => {
    const res = await fetch(ENDPOINTS.SEARCH_FORECAST("Argentina"));
    const data = await res.json();
    expect(data).not.toBeNull();
    expect(data).not.toBeUndefined();
    expect.arrayContaining(data);
  });

  test("Test weatherForecast in Bolivia", async () => {
    const res = await fetch(ENDPOINTS.SEARCH_FORECAST("Bolivia"));
    const data = await res.json();
    expect(data).not.toBeNull();
    expect(data).not.toBeUndefined();
    expect.arrayContaining(data);
  });

  test("Test weatherForecast in España", async () => {
    const res = await fetch(ENDPOINTS.SEARCH_FORECAST("España"));
    const data = await res.json();
    expect(data).not.toBeNull();
    expect(data).not.toBeUndefined();
    expect.arrayContaining(data);
  });
});
