import { ENDPOINTS } from "../config/endpoints";
import publicIp from "public-ip";

describe("Testing Ip", () => {
  test("Test Ip Geolocation", async () => {
    const res = await fetch(ENDPOINTS.IP_GEOLOCATION(await publicIp.v4()));
    const data = await res.json();
    expect(data).not.toBeNull();
    expect(data).not.toBeUndefined();
  });
});
