import { ENDPOINTS } from "../config/endpoints";

describe("Testing Api", () => {
  test("Test WeatherSearch in Argentina", async () => {
    const res = await fetch(ENDPOINTS.SEARCH("Argentina"));
    const data = await res.json();
    expect(data).not.toBeNull();
    expect(data).not.toBeUndefined();
    expect.arrayContaining(data);
  });

  test("Test WeatherSearch in Bolivia", async () => {
    const res = await fetch(ENDPOINTS.SEARCH("Bolivia"));
    const data = await res.json();
    expect(data).not.toBeNull();
    expect(data).not.toBeUndefined();
    expect.arrayContaining(data);
  });

  test("Test WeatherSearch in España", async () => {
    const res = await fetch(ENDPOINTS.SEARCH("España"));
    const data = await res.json();
    expect(data).not.toBeNull();
    expect(data).not.toBeUndefined();
    expect.arrayContaining(data);
  });
});
