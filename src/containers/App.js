import React, { useReducer } from "react";
import { initialState, reducer } from "../reducers/getWeather";
import Weather from "./weather";

export const Context = React.createContext();

function App() {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <Context.Provider value={{ state, dispatch }}>
      <Weather />
    </Context.Provider>
  );
}

export default App;
