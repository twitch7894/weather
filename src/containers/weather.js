import { Row, Col, Card } from "antd";
import WeatherSearch from "../components/weatherSearch";
import WeatherDetails from "../components/weatherDetails";
import WeatherForecast from "../components/weatherForecast";
import WeatherView from "../components/weatherView";

function Weather() {
  return (
    <div className="search_weather">
      <Row gutter={16}>
        <Col span={24}>
          <WeatherSearch />
        </Col>
        <Col lg={16} md={24} sm={24} xs={24}>
          <div className="item">
            <WeatherDetails />
          </div>
        </Col>
        <Col lg={8} md={24} sm={24} xs={24}>
          <div className="item">
            <Card title="Tiempo">
              <WeatherView />
            </Card>
          </div>
        </Col>
        <Col lg={16} md={24} sm={24} xs={24}>
          <div className="item">
            <WeatherForecast />
          </div>
        </Col>
      </Row>
    </div>
  );
}

export default Weather;
