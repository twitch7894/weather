import { FETCH_SUCCESS, FETCH_ERROR, INITIAL_FETCH } from "../constants";

const initialState = {
  loading: true,
  error: "",
  data: [],
};

const reducer = (state, action) => {
  switch (action.type) {
    case FETCH_SUCCESS:
      return {
        loading: false,
        data: action.payload,
        error: "",
      };
    case FETCH_ERROR:
      return {
        loading: true,
        data: [],
        error: "error",
      };
    case INITIAL_FETCH:
      return initialState;
    default:
      return state;
  }
};

export { initialState, reducer };
