import { useEffect, useContext, useCallback } from "react";
import { FETCH_SUCCESS } from "../constants";
import { ENDPOINTS } from "../config/endpoints";
import { Input, Card, notification } from "antd";
import publicIp from "public-ip";
import { Context } from "../containers/App";

const { Search } = Input;

export default function WeatherSearch() {
  const { dispatch } = useContext(Context);

  const openNotification = (placement, text) => {
    notification.error({
      message: `Error de Busqueda ${text}`,
      description: "La ciudad ingresada no se encuentra",
      placement,
    });
  };

  const SearchCity = useCallback(
    async (value) => {
      if (value !== "") {
        try {
          const res = await fetch(ENDPOINTS.SEARCH(value));
          if (!res.ok) throw new Error(res.statusText);
          const data = await res.json();
          dispatch({ type: FETCH_SUCCESS, payload: data });
        } catch (err) {
          console.log(err);
          openNotification("topRight", value);
        }
      }
    },
    [dispatch]
  );

  useEffect(() => {
    (async () => {
      try {
        const res = await fetch(ENDPOINTS.IP_GEOLOCATION(await publicIp.v4()));
        const data = await res.json();
        SearchCity(data.country);
      } catch (err) {
        console.log(err);
      }
    })();
  }, [SearchCity]);

  const onSearch = (value) => SearchCity(value);

  return (
    <Card title="Buscar">
      <Search
        placeholder="Buscar Ciudad"
        allowClear
        onSearch={onSearch}
        style={{ width: "100%" }}
      />
    </Card>
  );
}
