import React, { useContext } from "react";
import { Context } from "../containers/App";
import { ENDPOINTS } from "../config/endpoints";
import Loading from "./loading";
import { Description, Weekday } from "../utils/text";

export default function WeatherView() {
  const { state } = useContext(Context);
  const { data, loading } = state;

  return (
    <React.Fragment>
      {!loading ? (
        <div className="container-image">
          <div className="container-image-content">
            <p className="p-2">{Weekday[new Date(data.dt * 1000).getDay()]}</p>
            <img
              className="img"
              src={ENDPOINTS.IMAGE(data.weather[0].icon)}
              alt={ENDPOINTS.IMAGE(data.weather[0].id)}
            />
          </div>
          <div className="container-description">
            <p className="p-2">{Description[data.weather[0].description]}</p>
            <p className="p-2">
              {data.main.temp_max} / {data.main.temp_min} °C
            </p>
          </div>
        </div>
      ) : (
        <Loading />
      )}
    </React.Fragment>
  );
}
