import { Card, Select } from "antd";
import { Context } from "../containers/App";
import { useCallback, useContext, useEffect, useState } from "react";
import { ENDPOINTS } from "../config/endpoints";
import { Description, Weekday } from "../utils/text";
import Spin from "./loading";

export default function WeatherForecast() {
  const [value, setValue] = useState(null);
  const [info, setInfo] = useState([]);
  const [ForescastLoding, setSpinForescastLoding] = useState(true);
  const [hours, setHours] = useState(6);
  const { state } = useContext(Context);
  const { data, loading } = state;

  const { Option, OptGroup } = Select;

  function handleChange(data) {
    setHours(parseInt(data));
    const result = info.list.filter(
      (e) => new Date(e.dt_txt).getHours() === parseInt(data)
    );
    setValue(result);
  }

  const weekdayForecast = useCallback(
    (value) => {
      if (value) {
        const result = value.list.filter(
          (e) => new Date(e.dt_txt).getHours() === hours
        );
        setValue(result);
      }
    },
    [hours]
  );

  const type = (p) => (p <= 12 ? "AM" : "PM");

  useEffect(() => {
    (async () => {
      if (!loading) {
        try {
          const res = await fetch(ENDPOINTS.SEARCH_FORECAST(data.name));
          const result = await res.json();
          setInfo(result);
          setSpinForescastLoding(false);
          weekdayForecast(result);
        } catch (err) {
          console.log(err);
        }
      }
    })();
  }, [data, loading, weekdayForecast]);

  return (
    <Card title="Pronóstico del tiempo">
      {!ForescastLoding ? (
        <div>
          <div className="card-forescast-select">
            <p className="p-1">Horarios:</p>
            <Select
              defaultValue={`${hours}:00hs - ${type(hours)}`}
              style={{ width: 130, textAlign: "center" }}
              onChange={handleChange}
            >
              <OptGroup label="Horas">
                <Option value="3">3:00hs - AM</Option>
                <Option value="6">6:00hs - AM</Option>
                <Option value="9">9:00hs - AM</Option>
                <Option value="12">12:00hs - AM</Option>
                <Option value="15">15:00hs - PM</Option>
                <Option value="18">18:00hs - PM</Option>
                <Option value="21">21:00hs - PM</Option>
              </OptGroup>
            </Select>
          </div>
          <div className="container-forescast">
            {value !== null &&
              value.map((e) => (
                <div className="card-forescast" key={e.dt_txt}>
                  <p className="card-forescast-p">
                    {Weekday[new Date(e.dt_txt).getDay()]}
                  </p>
                  <div>
                    <img
                      className="img"
                      src={ENDPOINTS.IMAGE(e.weather[0].icon)}
                      alt={ENDPOINTS.IMAGE(e.weather[0].id)}
                    />
                  </div>
                  <div className="container-description">
                    <p className="p-2">
                      {Description[e.weather[0].description]}
                    </p>
                    <p className="p-2">
                      {e.main.temp} / {e.main.temp_min} °C
                    </p>
                  </div>
                </div>
              ))}
          </div>
        </div>
      ) : (
        <Spin />
      )}
    </Card>
  );
}
