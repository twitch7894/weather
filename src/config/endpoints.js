/**
 * API OPENWEATHERMAP
 */

const HOST = "https://api.openweathermap.org/data/2.5/";
const APPID = "6de56b37b8e1dffd1ad7bf4b8bd90a20";
const IP_GEOLOCATION = "http://ip-api.com/json";

export const ENDPOINTS = {
  SEARCH: (city) => `${HOST}weather?q=${city}&units=metric&appid=${APPID}`,
  IP_GEOLOCATION: (ip) => `${IP_GEOLOCATION}/${ip}`,
  SEARCH_FORECAST: (city) => `${HOST}forecast?q=${city}&units=metric&appid=${APPID}`,
  IMAGE: (value) => `http://openweathermap.org/img/wn/${value}@2x.png`,
};
